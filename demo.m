
clear ;clc;
rng('default');
run matconvnet/matlab/vl_setupnn ;
addpath matconvnet/examples ;

sigma=25;
if sigma==15
    load('sigma15.mat');%15
elseif sigma==25
    load('sigma25.mat');%25
elseif sigma==45
    load('sigma45.mat');%45
elseif sigma==75
    load('sigma75.mat');%75
else
    disp('Please use sigma=[15,25,45,75] for this demo.')
end
net=dagnn.DagNN.loadobj(net);
net.mode='test';
net.move('gpu');
lastL='sum';

% load image
img_path='Set14';
% img_path='Kodak';
imgPath=[];
exc={'*.jpg' '*.png'};
for e=1:length(exc)
    imgPath=cat(1,imgPath,dir(fullfile(img_path,exc{e})));
end
num=length(imgPath);
randn('seed',0);
pp=zeros(num,1);
ss=zeros(num,1);

FSIM=zeros(num,1);
psnr_all=0;
ssim_all=0;
disp('processing..');
for i=1:num
    name=fullfile(img_path,imgPath(i).name);
    im_clean = imread(name);
    if size(im_clean,3)==3
        im_clean=rgb2gray(im_clean);
    end
    im_clean=double(im_clean)/255;
    [h,w,s]=size(im_clean);
    % add noise
    im_noisy = im_clean + sigma/255*randn(size(im_clean));
    im=im_noisy;
    
    % denoise
    im=single(im);
    im=im2uint8(im);im=im2single(im);
    FigHandle=figure(1) ;
    set(gcf, 'name', ['Evaluation ', num2str(i),'/',num2str(num)]) ;
    colormap gray ;
    subplot(1,3,1) ; imagesc(im_clean) ;
    axis image off ;
    title('clean') ;
    subplot(1,3,2) ; imagesc(im) ;
    axis image off ;
    title('noisy') ;
    
    y_e_t=gpuArray(im);
% y_e_t=im;
    net.eval({'input', y_e_t}) ;
    predVar=net.getVarIndex(lastL);
    preds0 = gather(net.vars(predVar).value) ;
    res=preds0;
    
    im_clean=single(im_clean*255);res=res*255;im=im*255;
    [psnr,ssim]=PSNRSSIM(im_clean,res);
    psnr_all=psnr_all+psnr;
    ssim_all=ssim_all+ssim;

    subplot(1,3,3) ; imagesc(res) ;
    axis image off ;
    title(['Ours' num2str(psnr)]) ;
    
    drawnow;
    pp(i,1)=psnr;
    ss(i,1)=ssim;
end
psnr_all=psnr_all/num
ssim_all=ssim_all/num
pp=gather(pp);
ss=gather(ss);