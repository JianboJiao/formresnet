# FormResNet: Formatted Residual Learning for Image Restoration

By [Jianbo Jiao](https://jianbojiao.com/), [Wei-Chih Tu](https://sites.google.com/site/wctu1009/), [Shengfeng He](http://www.shengfenghe.com/), [Rynson W. H. Lau](http://www.cs.cityu.edu.hk/~rynson/).


### Introduction

This repository contains the codes and models described in the paper "[FormResNet: Formatted Residual Learning for Image Restoration](http://openaccess.thecvf.com/content_cvpr_2017_workshops/w12/papers/Jiao_FormResNet_Formatted_Residual_CVPR_2017_paper.pdf)" . FormResNet is an end-to-end network for image restoration that takes advantage of residual learning.

### Usage

0. Install [MatConvNet](http://www.vlfeat.org/matconvnet/) and compile according to the [instructions](http://www.vlfeat.org/matconvnet/install/)
1. Run the [demo.m](demo.m) for the evaluation on Gaussian image denoising
2. The code [demo_derain.m](demo_derain.m) is a demo for image deraining

### Citation

If you find these codes and models useful in your research, please cite:

	@InProceedings{Jianbo_2017_CVPR_Workshops,
		author = {Jianbo Jiao, Wei-chi Tu, Shengfeng He, Rynson W. H. Lau},
		title = {FormResNet: Formatted Residual Learning for Image Restoration},
		booktitle = {The IEEE Conference on Computer Vision and Pattern Recognition (CVPR) Workshops},
		month = {July},
		year = {2017}
	}
**References**

[1] He K, Zhang X, Ren S, Sun J. [Deep residual learning for image recognition](http://openaccess.thecvf.com/content_cvpr_2016/papers/He_Deep_Residual_Learning_CVPR_2016_paper.pdf). InProceedings of the IEEE conference on computer vision and pattern recognition 2016.

[2] Li Y, Tan RT, Guo X, Lu J, Brown MS. [Rain streak removal using layer priors](http://yu-li.github.io/). InProceedings of the IEEE Conference on Computer Vision and Pattern Recognition 2016.

[3] [Kodak Lossless True Color Image Suite](http://r0k.us/graphics/kodak/index.html)

[Here](https://github.com/MingtaoGuo/FormResNet-Denoise-Gaussian-noise-TensorFlow) is a TensorFlow implementation, thanks MarTinGuo.

If you have any questions please email the [authors](mailto:jiaojianbo.i@gmail.com)