
clear ;
rng('default');
run matconvnet/matlab/vl_setupnn ;
addpath matconvnet/examples ;

load('derain.mat');
net=dagnn.DagNN.loadobj(net);
net.mode='test';
net.move('gpu');
lastL='sum';

% load an image
img_path='test_rain.png';
im_rain_rgb = imread(img_path);
if size(im_rain_rgb,3)==3
    im_rain=rgb2ycbcr(im_rain_rgb);
    im=im_rain(:,:,1);
end
im=im2single(im);

% derain
FigHandle=figure(1) ;
set(gcf, 'name', 'Deraining') ;
colormap gray ;
subplot(1,2,1) ; imagesc(im_rain_rgb) ;
axis image off ;
title('rainy image') ;

y_e_t=gpuArray(im);
% y_e_t=im;
net.eval({'input', y_e_t}) ;
predVar=net.getVarIndex(lastL);
preds0 = gather(net.vars(predVar).value) ;
res=preds0;

pred_rain(:,:,1)=uint8(res*255);
pred_rain(:,:,2:3)=im_rain(:,:,2:3);
pred_rain=ycbcr2rgb(pred_rain);

subplot(1,2,2) ; imagesc(pred_rain) ;
axis image off ;
title(['Ours']) ;
drawnow;